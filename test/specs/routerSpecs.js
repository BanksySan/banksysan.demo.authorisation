var chai = require('chai'),
    expect = chai.expect,
    sinon = require('sinon'),
    sinonChai = require("sinon-chai"),
    log = console.log;

chai.should();
chai.use(sinonChai);

describe('The router', function() {
    var router;

    describe('when adding routes', function() {

        it('should not invoke the action', function() {
            var mockAction = sinon.spy(),
                dummyMethod = 'dummy-method',
                dummyPath = 'dummy-path';

            router = require('../../app/router.js');

            router.add(dummyMethod, dummyPath, mockAction);

            mockAction.should.not.be.called;
        });
    });

    describe('when getting a route', function() {
        var dummyMethod,
            dummyPath,
            dummyRoutes;

        beforeEach(function() {
            var i,
                dummyRoute,
                router = require('../../app/router.js');

            dummyRoutes = [{
                method: 'dummy-method-1',
                path: 'dummy-path-1',
                action: sinon.spy()
            }, {
                method: 'dummy-method-2',
                path: 'dummy-path-2',
                action: sinon.spy()
            }, {
                method: 'dummy-method-3',
                path: 'dummy-path-3',
                action: sinon.spy()
            }];

            for (i = 0; i < dummyRoutes.length; i++) {
                dummyRoute = dummyRoutes[i];

                router.add(dummyRoute.method, dummyRoute.path, dummyRoute.action);
            }
        });

        it('should pass the correct action to the callback.', function() {
            var searchedRoute = dummyRoutes[1],
                mockCallback = sinon.spy();

            router.action(searchedRoute.method, searchedRoute.path, mockCallback);

            expect(mockCallback.args.length)
                .to.be.equal(1);
            expect(mockCallback.calledWith(dummyRoutes[1].action))
                .to.be.true;
        });
    });
});
