var sinon = require('sinon'),
    log = console.log;

describe('dummy tests: ', function() {
    var dummyRoutes;

    it('Why are the spy methods undefined?', function() {
        var mockCallback = sinon.spy(),
            dummyRoutes = [{
                dummyProp: 'dummy-method-1',
                method: 'boogus'
            }, {
                dummyProp: 'dummy-method-2',
                method: sinon.spy(function() {
                    log('Yowsers')
                })
            }];
        log('mockCallback: ' + mockCallback);
        log('sinon.spy(): ' + sinon.spy());
    });
});
