module.exports = (function() {

    var _ = require('underscore')
        ._,
        routes = [];

    return {
        add: function(method, path, action) {
            routes.push({
                method: method,
                path: path,
                callback: action
            });

        },

        action: function(method, path, callback) {
            var route = _.find(routes, function(route) {
                return route.method === method && route.path === path;
            });

            if (route)
                callback(route.callback);
            else {
                throw new Error('Cannot find an action matching method "' + method + '" and path "' + path + "'.");
            }
        }
    };
}());
