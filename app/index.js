var http = require('http'),
    router = require('./router'),
    routeConfig = require('./routeConfig'),
    PORT_NUMBER = 8000;

console.log('Initialising server...');

routeConfig.forEach(function(item) {
    router.add(item.method, item.path, item.action);
});

http.createServer(function(request, response) {
        var method = request.method,
            path = request.url;

        console.log('recieved ' + method + ' ' + path);

        request.on('data', function(chunk) {
            router.action(method, path, function(action) {
                console.log('typeof action: ' + typeof action);

                response.writeHead(200, {
                    'Content-Length': 4,
                    'Content-Type': 'text/plain'
                });
                response.write('Yup!');
                response.end();
            });
        });
    })
    .listen(PORT_NUMBER);

console.log('>>> Node listening on port: ' + PORT_NUMBER);
