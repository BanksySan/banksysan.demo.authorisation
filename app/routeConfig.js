module.exports = (function() {

    var GET = 'GET';

    return [{
        method: GET,
        path: '/',
        action: function() {
            console.log('Return the root GET action here');
        }
    }];
}());
