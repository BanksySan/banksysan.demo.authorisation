var routes = routes || {};

module.exports.add = function(method, path, callback) {
    routes[method] = routes[method] || [];

    var httpMethod = routes[method];

    httpMethod.push({
        path: path,
        callback: callback
    });
};

module.exports.get = function(method, path) {
    var i, methodRoutes = routes[method];
    for (i = 0; i < methodRoutes.length; i++) {
        if (methodRoutes[i].path === path) {
            return methodRoutes[i].callback;
        }
    }
    throw "Could not locate route for " + method + ':' + path + '.';
};
