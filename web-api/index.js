var http = require('http'),
    mongoose = require('mongoose'),
    authorisationItemTemplate = require('./templates/authorisation-item.js'),
    router = require('./router.js'),
    port = 3030,
    authorisationItemSchema = mongoose.Schema(authorisationItemTemplate.template),
    authorisationModel = mongoose.model('authorisation-items', authorisationItemSchema);

router.add('get', '/', function(item) {
    console.log(JSON.stringify(item, null, 4));
});


var post = function(req) {
    req.on('data', function(chunk) {
        var authorisationItem, newDocument;

        newDocument = JSON.parse(chunk.toString());

        authorisationItemTemplate.validate(newDocument);

        authorisationItem = new authorisationModel({
            origin: newDocument.origin,
            document: JSON.stringify(newDocument.document),
            tags: newDocument.tags
        });

        authorisationItem.save(function(err) {
            if (err) {
                console.error(err);
                throw err;
            } else {
                console.log('Saved!');
            }
        });
    });

    console.log('POST received.');
};

var get = function(path) {
    console.log('GET received.  URL: "' + path + '"');

    router.get('get', path)();

};

mongoose.connect('mongodb://localhost/authorisation-dev');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'Mongo (Mongoose) connection error...'));
db.once('open', function callback() {
    console.log('authorisation-dev db opened');
});

http.createServer(function(request, response) {


    switch (request.method) {
        case 'GET':
            get(request.url);
            break;
        case 'POST':
            post(request, request.body);
            break;
        default:
            throw 'Unsupported method "' + request.method + "'";
    }

    var body = 'hello world';
    response.writeHead(200, {
        'Content-Length': body.length,
        'Content-Type': 'text/plain'
    });
    response.write(body);
    response.end();
}).listen(port);


console.log('Node.JS listening on ' + port);
