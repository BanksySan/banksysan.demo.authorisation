var authorisationItem = {};

authorisationItem.template = exports.template || {
    origin: String,
    document: String,
    tags: [String]
};

authorisationItem.validate = function(data) {
    var errors = [];

    if (!data) {
        errors.push('Authorisation item cannot be null');
    } else {
        if (!data.origin) {
            errors.push('origin cannot be null');
        }

        if (!data.document) {
            errors.push('document cannot be null');
        }
    }
};

module.exports = authorisationItem;
